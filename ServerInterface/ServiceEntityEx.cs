using System;
using WindowServices.Interface;

namespace WindowServices.Server.Interface
{
    [Serializable]
    public class ServiceEntityEx : ServiceEntity
    {
        public bool Inheritance { get; set; }

        public User UserInfo { get; set; }

        /// <summary>
        /// 表示是否已经编辑
        /// </summary>
        public bool IsEdit { get; set; }
        /// <summary>
        /// 表示是否要删除、更新、新增
        /// </summary>
        public ServiceState State { get; set; }

        public string Key
        {
            get
            {
                return string.Format("{0}-{1}", Id, Version);
            }
        }
    }
}