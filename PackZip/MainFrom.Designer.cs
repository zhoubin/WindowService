﻿namespace PackZip
{
    partial class MainFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.cboFiles = new System.Windows.Forms.ComboBox();
            this.cbotypes = new System.Windows.Forms.ComboBox();
            this.txtSavePath = new System.Windows.Forms.TextBox();
            this.btnSelectpath = new System.Windows.Forms.Button();
            this.btnSelectSaveFile = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label8 = new System.Windows.Forms.Label();
            this.lblDesc = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Path";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "File";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 0;
            this.label3.Text = "Type";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 250);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "Description";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 287);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "SavePath";
            // 
            // lblID
            // 
            this.lblID.Location = new System.Drawing.Point(105, 146);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(565, 23);
            this.lblID.TabIndex = 1;
            this.lblID.Text = "label8";
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(105, 180);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(565, 12);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "label8";
            // 
            // lblVersion
            // 
            this.lblVersion.Location = new System.Drawing.Point(105, 216);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(565, 12);
            this.lblVersion.TabIndex = 1;
            this.lblVersion.Text = "label8";
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(107, 32);
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(563, 21);
            this.txtPath.TabIndex = 2;
            // 
            // cboFiles
            // 
            this.cboFiles.FormattingEnabled = true;
            this.cboFiles.Location = new System.Drawing.Point(107, 71);
            this.cboFiles.Name = "cboFiles";
            this.cboFiles.Size = new System.Drawing.Size(563, 20);
            this.cboFiles.TabIndex = 3;
            this.cboFiles.SelectedIndexChanged += new System.EventHandler(this.cboFiles_SelectedIndexChanged);
            // 
            // cbotypes
            // 
            this.cbotypes.FormattingEnabled = true;
            this.cbotypes.Location = new System.Drawing.Point(107, 108);
            this.cbotypes.Name = "cbotypes";
            this.cbotypes.Size = new System.Drawing.Size(563, 20);
            this.cbotypes.TabIndex = 3;
            this.cbotypes.SelectedIndexChanged += new System.EventHandler(this.cbotypes_SelectedIndexChanged);
            // 
            // txtSavePath
            // 
            this.txtSavePath.Location = new System.Drawing.Point(107, 284);
            this.txtSavePath.Name = "txtSavePath";
            this.txtSavePath.Size = new System.Drawing.Size(563, 21);
            this.txtSavePath.TabIndex = 2;
            // 
            // btnSelectpath
            // 
            this.btnSelectpath.Location = new System.Drawing.Point(677, 29);
            this.btnSelectpath.Name = "btnSelectpath";
            this.btnSelectpath.Size = new System.Drawing.Size(75, 23);
            this.btnSelectpath.TabIndex = 4;
            this.btnSelectpath.Text = "...";
            this.btnSelectpath.UseVisualStyleBackColor = true;
            this.btnSelectpath.Click += new System.EventHandler(this.btnSelectpath_Click);
            // 
            // btnSelectSaveFile
            // 
            this.btnSelectSaveFile.Location = new System.Drawing.Point(677, 282);
            this.btnSelectSaveFile.Name = "btnSelectSaveFile";
            this.btnSelectSaveFile.Size = new System.Drawing.Size(75, 23);
            this.btnSelectSaveFile.TabIndex = 4;
            this.btnSelectSaveFile.Text = "...";
            this.btnSelectSaveFile.UseVisualStyleBackColor = true;
            this.btnSelectSaveFile.Click += new System.EventHandler(this.btnSelectSaveFile_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(355, 331);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Pack";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "zip";
            this.saveFileDialog1.Filter = "zip|*.zip";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 12);
            this.label8.TabIndex = 0;
            this.label8.Text = "Version";
            // 
            // lblDesc
            // 
            this.lblDesc.Location = new System.Drawing.Point(105, 250);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(565, 12);
            this.lblDesc.TabIndex = 1;
            this.lblDesc.Text = "label8";
            // 
            // MainFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(787, 392);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnSelectSaveFile);
            this.Controls.Add(this.btnSelectpath);
            this.Controls.Add(this.cbotypes);
            this.Controls.Add(this.cboFiles);
            this.Controls.Add(this.txtSavePath);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.lblDesc);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainFrom";
            this.Text = "插件打包程序";
            this.Load += new System.EventHandler(this.MainFrom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.ComboBox cboFiles;
        private System.Windows.Forms.ComboBox cbotypes;
        private System.Windows.Forms.TextBox txtSavePath;
        private System.Windows.Forms.Button btnSelectpath;
        private System.Windows.Forms.Button btnSelectSaveFile;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblDesc;
    }
}

