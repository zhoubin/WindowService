namespace WindowServices.Service.Command.Task
{
    public interface ITask : ICommand
    {
        int Order { get; }
        TaskState State { get; }

    }
}