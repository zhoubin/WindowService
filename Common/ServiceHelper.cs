using System;
using System.Collections.Generic;
using WindowServices.Server.Interface;

namespace WindowServices.Common
{
    public static class ServiceHelper
    {
        private static readonly string Configfile = string.Format("{0}\\ServiceList.config", AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\'));
        
        public static List<ServiceEntityEx> GetAllServiceEntity()
        {
            return GetServiceEntity(Guid.Empty, null);
        }

        public static void AddServiceEntity(ServiceEntityEx se)
        {
            using (var helper = CreateConfigHelper())
            {
                helper.AddServiceEntity(se);
            }
        }



        public static void DelServiceEntity(List<ServiceEntityEx> list)
        {
            using (var helper = CreateConfigHelper())
            {
                helper.DelServiceEntity(list);
            }
        }

       

        public static void DelServiceEntity(Guid id, Version version)
        {
            using (var helper = CreateConfigHelper())
            {
                helper.DelServiceEntity(id, version);
            }
        }

        public static void UpdateServiceEntity(ServiceEntityEx se)
        {
            using (var helper = CreateConfigHelper())
            {
                helper.UpdateServiceEntity(se);
            }
        }

        public static List<ServiceEntityEx> GetServiceEntity(Guid id, Version version)
        {

            using (var helper = CreateConfigHelper())
            {
                return helper.GetServiceEntity(id, version);
            }

        }

        private static ServiceConfig CreateConfigHelper()
        {
            return new ServiceConfig(Configfile);
        }
    }
}