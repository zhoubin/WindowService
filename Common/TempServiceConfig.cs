using System;
using System.IO;

namespace WindowServices.Common
{
    public class TempServiceConfig : ServiceConfig
    {
        public static readonly string ConfigFile = string.Format("{0}\\TempServiceList.config",
            AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\'));
        public TempServiceConfig(bool deleteFile)
            : base(ConfigFile)
        {
            if (!deleteFile)
            {
                return;
            }
            if (File.Exists(ConfigFile))
            {
                File.Delete(ConfigFile);
            }

            File.Copy(ConfigFile + ".Template", ConfigFile);
        }


    }
}