﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using WindowServices.Server.Interface;
using WindowServices.Common.Properties;

namespace WindowServices.Common
{
    public class ServiceSection : System.Configuration.IConfigurationSectionHandler
    {
        public static readonly string SectionName = "Services";
        #region IConfigurationSectionHandler Members

        /// <summary>
        /// 加载出错，返回null
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, XmlNode section)
        {
            if (section.Name != SectionName)
            {
                return null;
            }
            var list = new List<ServiceEntityEx>();

            try
            {
                foreach (var node in
                    section.ChildNodes.Cast<XmlNode>().Where(node => String.Compare(node.Name, "service", StringComparison.OrdinalIgnoreCase) == 0))
                {
                    if (node.Attributes == null)
                    {
                        continue;
                    }
                    var se = new ServiceEntityEx
                    {
                        State = ServiceState.None,
                        Id = new Guid(node.Attributes["id"].Value),
                        Name = node.Attributes["name"].Value,
                        Type = node.Attributes["type"].Value,
                        Version = new Version(node.Attributes["version"].Value),
                        IsEdit = String.CompareOrdinal("true", node.Attributes["edit"].Value) == 0,
                        Inheritance = String.CompareOrdinal("true", node.Attributes["inheritance"].Value) == 0,
                        UserInfo = new User()
                    };

                    if (!se.Inheritance)
                    {
                        foreach (XmlNode child in
                            node.ChildNodes.Cast<XmlNode>().Where(child => String.Compare(child.Name, "inheritance", StringComparison.OrdinalIgnoreCase) == 0))
                        {
                            if (child.Attributes == null)
                            {
                                continue;
                            }

                            switch (child.Name.ToLower())
                            {
                                case "domain":
                                    se.UserInfo.Domain = child.Attributes["domain"].Value;
                                    break;
                                case "userid":
                                    se.UserInfo.UserId = child.Attributes["userid"].Value;
                                    break;
                                case "password":
                                    se.UserInfo.PassWord = child.Attributes["password"].Value;
                                    break;
                            }
                        }
                    }

                    list.Add(se);
                }
            }
            catch (Exception ee)
            {
                LogHelper.WriteEntity("Windows.Config", string.Format(Resources.ReadConfigError, ee), EventType.ReadConfigError, System.Diagnostics.EventLogEntryType.Error);
                list = null;
            }

            return list;
        }

        #endregion
    }
}
